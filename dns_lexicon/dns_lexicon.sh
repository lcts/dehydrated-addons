#!/bin/bash
#
# Deploy a DNS challenge using dns-lexicon
#

set -e
set -u
set -o pipefail

# use default config file location if DNS_LEXICON_CONF is not set
DNS_LEXICON_CONF=${DNS_LEXICON_CONF:-"/etc/dehydrated/conf.d/dns_lexicon.conf"}

# source config file
if [[ -f $DNS_LEXICON_CONF ]]; then
    set -o allexport
    source $DNS_LEXICON_CONF
    set +o allexport
fi

# set default values for configuration options
PROVIDER=${PROVIDER:-}
PROVIDER_UPDATE_WAIT=${PROVIDER_UPDATE_WAIT:-"30"}
PROVIDER_UPDATE_TIMEOUT=${PROVIDER_UPDATE_TIMEOUT:-"300"}
VERBOSE=${VERBOSE:-}

# internal variables
# formatted output
print_prefix="    (dns_lexicon)"
debug_prefix="debug:"


_print() {
    echo "$print_prefix $@"
}

_print_debug() {
    if [[ $VERBOSE ]]; then
        _print "$debug_prefix $@"
    fi
}

deploy_challenge() {
    # This hook is called once for every domain chain that needs to be
    # validated, including any alternative names.

    local chain=($@)
    for ((i=0; i < $#; i+=3)); do
        local domain="${chain[i]}" token_filename="${chain[i+1]}" token_value="${chain[i+2]}"

        _print "Deploy challenge token for domain $domain."
        _print_debug "Token: $token_filename"
        _print_debug "Token value: $token_value"

        lexicon $PROVIDER create $domain TXT \
            --name="_acme-challenge.${domain}." \
            --content="$token_value" > /dev/null 2>&1
    done

    _print "Waiting ${PROVIDER_UPDATE_WAIT}s for changes to propagate ..."
    sleep $PROVIDER_UPDATE_WAIT
}

clean_challenge() {
    # This hook is called after attempting to validate each domain
    # chain, whether or not validation was successful.

    local chain=($@)
    for ((i=0; i < $#; i+=3)); do
        local domain="${chain[i]}" token_filename="${chain[i+1]}" token_value="${chain[i+2]}"

        _print "Remove challenge token for domain $domain"
        _print_debug "Token: $token_filename"
        _print_debug "Token value: $token_value"

        lexicon $PROVIDER delete $domain TXT \
            --name="_acme-challenge.${domain}." \
            --content="$token_value" > /dev/null 2>&1
    done
}

# execute handler function if known, otherwise do nothing
handler=$1; shift;
if [[ "$handler" =~ ^(deploy_challenge|clean_challenge)$ ]]; then
    if [[ $PROVIDER ]]; then
        _print_debug "Using provider $PROVIDER."
        "$handler" "$@"
    else
        _print "Missing configuration parameter: PROVIDER"
    fi
else
    _print_debug "Hook $handler is unused."
fi
