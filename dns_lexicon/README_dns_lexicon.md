# dehydrated-dns-lexicon

Hook to allow dehydrated to post DNS-01 challenges using
the python lexicon library.

## Installation
Copy both `dns_lexicon.sh` and `dns_lexicon.conf` to a directory accessible to
dehydrated. The default locations are `<dehydrated config dir>/` (usually
`/etc/dehydrated`) for the script and `<dehydrated config dir>/conf.d/` for
the config file.

## Configuration
By default, the script will look for a config file 
`<dehydrated config dir>/conf.d/dns_lexicon.conf`. Different locations
can be configured by setting the `DNS_LEXICON_CONF` environment variable.

This hook uses the API provided by the lexicon CLI to interact with
DNS providers.

Configure the DNS provider by setting the `PROVIDER` variable (see 
https://dns-lexicon.readthedocs.io/en/latest/configuration_reference.html
for list of supported providers and associated configuration options.

DNS provider-specific settings like credentials can be configured by setting
`LEXICON_<PROVIDER>_<VARIABLE>` vars. See
https://dns-lexicon.readthedocs.io/en/latest/user_guide.html#environmental-variables
for details.
