#!/bin/bash
#
# Proxy hook to allow dehydrated to execute multiple hooks
#

set -e
set -u
set -o pipefail

# use default config file location if HOOKRUNNER_CONF is not set
HOOKRUNNER_CONF=${HOOKRUNNER_CONF:-"/etc/dehydrated/conf.d/hookrunner.conf"}

# internal variables
handler="$1"
print_prefix="    (hookrunner - $handler)"

_print() {
    echo "$print_prefix $@"
}

run_hooks() {
    if [[ -f $HOOKRUNNER_CONF ]]; then
        # change to dehydrated working dir
        pushd $BASEDIR 2<&1 > /dev/null
        # run hooks using bash
        while read hook; do
            [[ "$hook" == '' ]] || [[ "$hook" =~ ^#.* ]] && continue
            if [[ -x "$hook" ]]; then
                _print "Executing ${hook}..."
                bash "$hook" "$@"
            else
                _print "Error: Not an executable file: $hook"
            fi
        done < $HOOKRUNNER_CONF
        # return to previous dir
        popd 2<&1 > /dev/null
    else
        _print "Error: Configuration file not found: $HOOKRUNNER_CONF"
    fi
}

if [[ "${handler}" =~ ^(deploy_challenge|clean_challenge|sync_cert|deploy_cert|deploy_ocsp|unchanged_cert|invalid_challenge|request_failure|generate_csr|startup_hook|exit_hook)$ ]]; then
    run_hooks "$@"
fi
