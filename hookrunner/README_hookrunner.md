# dehydrated-hookrunner

Proxy hook for dehydrated that allows triggering of 
multiple dehydrated hooks in sequence.

## Installation
Copy both `hookrunner.sh` and `hookrunner.conf` to a directory accessible to
dehydrated. The default locations are `<dehydrated config dir>/` (usually
`/etc/dehydrated`) for the script and `<dehydrated config dir>/conf.d/` for
the config file.

## Configuration
By default, the script will look for its config file in 
`<dehydrated config dir>/conf.d/hookrunner.conf`. Different locations
can be configured by setting the `HOOKRUNNER_CONF` environment variable.

Hooks listed in the config file are executed order. Hooks can be
specified as absolute paths or relative to `<dehydrated config dir>`.
Empty lines and lines starting with `#` are ignored. Hooks must be
executable.
