# dehydrated-deploy_pki

Hook to deploy updated certificates to PKI directories & trigger reload commands if needed.

## Installation
Copy both `deploy_pki.sh` and `deploy_pki.conf` to a directory accessible to
dehydrated. The default locations are `<dehydrated config dir>/` (usually
`/etc/dehydrated`) for the script and `<dehydrated config dir>/conf.d/` for
the config file.

## Configuration
By default, the script will look for a config file 
`<dehydrated config dir>/conf.d/deploy_pki.conf`. Different locations can be configured by
setting the `DEPLOY_PKI_CONF` environment variable.

By default, certificates are installed to `/etc/pki/tls`, but can be configured via the
config file.

A reload command to trigger if certificates have been updated can be set using the `RELOAD_CMD`
variable.
