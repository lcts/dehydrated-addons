#!/bin/bash
#
# Deploy updated certificates to target directories & trigger reload commands
#

set -e
set -u
set -o pipefail
#shopt  -s extglob

# use default config file location if DEPLOY_PKI_CONF is not set
DEPLOY_PKI_CONF=${DEPLOY_PKI_CONF:-"/etc/dehydrated/conf.d/deploy_pki.conf"}

# source config file
if [[ -f $DEPLOY_PKI_CONF ]]; then
    set -o allexport
    source $DEPLOY_PKI_CONF
    set +o allexport
fi

# set default values for configuration options
# directories
PKI_BASEDIR=${PKI_BASEDIR:-"/etc/pki/tls"}
PKI_CERTDIR=${PKI_CERTDIR:-"${PKI_BASEDIR}/certs"}
PKI_KEYDIR=${PKI_KEYDIR:-"${PKI_BASEDIR}/private"}
PKI_CHAINDIR=${PKI_CERTDIR:-"$PKI_CERTDIR"}
PKI_FULLCHAINDIR=${PKI_CERTDIR:-"$PKI_CERTDIR"}
PKI_OCSPDIR=${PKI_OCSPDIR:-"$PKI_CERTDIR"}
# options
CERT_PER_CA=${CERT_PER_CA:-}
RELOAD_CMD=${RELOAD_CMD:-}
VERBOSE=${VERBOSE:-}

# internal variables
# output formatting
print_prefix="    (deploy_pki)"
debug_prefix="debug:"
tmpdir="/tmp"

# CAs
le_staging_url="https://acme-staging-v02.api.letsencrypt.org/directory"
le_url="https://acme-v02.api.letsencrypt.org/directory"
caname=''

# this is currently not exported by dehydrated, so we always set
# letsencrypt
CA="$le_url"

_print() {
    echo "$print_prefix $@"
}
_print_debug() {
    if [[ $VERBOSE ]]; then
        _print "$debug_prefix $@"
    fi
}

get_ca() {
    case $CA in
        $le_url)
            caname="letsencrypt"
        ;;
        $le_staging_url)
            caname="letsencrypt-staging"
        ;;
    esac
}

deploy_cert() {
    # This hook is called once for each certificate that has been
    # produced.

    local chain=($@)
    get_ca
    _print_debug "CA is: $caname"
    for ((i=0; i < $#; i+=6)); do
        local domain="${chain[i]}"
        local keyfile="${chain[i+1]}" certfile="${chain[i+2]}" fullchainfile="${chain[i+3]}" chainfile="${chain[i+4]}"
        local timestamp="${chain[i+5]}"

        _print "Deploying certificates for ${domain}..."
        _print_debug "Copying key file to ${PKI_KEYDIR}/${domain}.key"
        cp "$keyfile" "${PKI_KEYDIR}/${domain}.key"
        _print_debug "Copying cert file to ${PKI_CERTDIR}/${domain}.pem"
        cp "$certfile" "${PKI_CERTDIR}/${domain}.pem"
        _print_debug "Copying fullchain file to ${PKI_FULLCHAINDIR}/${domain}-fullchain.pem"
        cp "$fullchainfile" "${PKI_FULLCHAINDIR}/${domain}-fullchain.pem"
        if [[ $CHAIN_PER_CA ]] && [[ $caname ]]; then
            _print_debug "Using per-ca chain file."
            _print_debug "Copying chain file to ${PKI_CHAINDIR}/${caname}.pem"
            cp "$chainfile" "${PKI_CHAINDIR}/${caname}.pem"
        else
            _print_debug "Copying chain file to ${PKI_CHAINDIR}/${domain}-chain.pem"
            cp "$chainfile" "${PKI_CHAINDIR}/${domain}-chain.pem"
        fi
    done
    touch "${tmpdir}/cert_is_updated"
}

deploy_ocsp() {
    # This hook is called once for each updated ocsp stapling file that has
    # been produced.

    local chain=($@)
    for ((i=0; i < $#; i+=3)); do
        local domain="${chain[i]}" ocspfile="${chain[i+1]}" timestamp="${chain[i+2]}"

        _print "Deploying OCSP file for $domain..."
        _print_debug "Copying OCSP file to ${PKI_OCSPDIR}/${domain}-ocsp.der"
        cp "$ocspfile" "${PKI_OCSPDIR}/${domain}-ocsp.der"
    done
    touch "${tmpdir}/cert_is_updated"
}

exit_hook() {
    # This hook is called at the end of the cron command
    local error="${1:-}"
    if [[ ! $error ]] && [[ -f "${tmpdir}/cert_is_updated" ]]; then
        if [[ $RELOAD_CMD ]]; then
            _print "Executing reload commands..."
            _print_debug "Reload command is: $RELOAD_CMD"
            eval "$RELOAD_CMD"
        fi
        rm "${tmpdir}/cert_is_updated"
    fi
}

# execute handler function if known, otherwise do nothing
handler=$1; shift;
if [[ "$handler" =~ ^(deploy_cert|deploy_ocsp|exit_hook)$ ]]; then
        "$handler" "$@"
else
    _print_debug "Hook $handler is unused."
fi
