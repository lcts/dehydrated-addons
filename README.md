# dehydrated-addons

Addons for the dehydrated ACME client

 - systemd service & timer units for automatic certificate updates using systemd
 - hookrunner, a proxy hook to trigger multiple dehydrated hooks sequentially
 - dns_lexicon, a hook to set DNS-01 challenges with various DNS providers using
   lexicon
 - deploy_pki, a hook to deploy certificates to configurable directories and
   trigger reload commands on cert update
