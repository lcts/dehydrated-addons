# dehydrated-systemd

Systemd timer to automatically update certificates
using dehydrated. Can be used in place of dehydrated's
cron jobs.

## Installation
Copy `dehydrated-renew.service` and `dehydrated-renew.timer` to
`/etc/systemd/system` make systemd aware of them by running
`systemctl daemon-reload`. If you don't use the environment file,
remove the `EnvironmentFile=` line from `dehydrated-renew.service`,
otherwise, copy `dehydrated-renew.sysconfig` to
`/etc/sysconfig/dehydrated-renew`

## Configuration
By default, the service will execute `dehydrated --cron` without
any additional options. Further command line options can be passed
by setting `DEHYDRATED_OPTS` in `/etc/sysconfig/dehydrated-renew`.
